const fs = require("fs");
const path = require("path");

let hanjiCraft = {
    category: [
        {
            name: "문영함",
            details_category: []
        },
        {
            name: "보석함",
            details_category: []
        },
        {
            name: "손바구니",
            details_category: []
        },
        {
            name: "연필꽂이",
            details_category: ["소형", "중형"]
        },
        {
            name: "육각접시",
            details_category: []
        },
        {
            name: "차통",
            details_category: ["소형", "중형"]
        },
        {
            name: "커피함",
            details_category: []
        },
        {
            name: "팔각상",
            details_category: []
        },
        {
            name: "편지꽂이",
            details_category: []
        },
        {
            name: "화병",
            details_category: []
        },
        {
            name: "휴지 갑",
            details_category: ["소형", "일반"]
        },
    ]
};
let knitting = {
    category: [
        {
            name: "가방",
            details_category: ["숄더백", "백팩"]
        },
        {
            name: "모자",
            details_category: ["여름 챙 모자", "유아용 털 모자"]
        },
        {
            name: "목토시",
            details_category: []
        },
        {
            name: "휴지 갑",
            details_category: ["소형", "일반"]
        },
    ]
};
module.exports.productsInfo = (req, res, next) => {
    let productsInfo = {};
    if (req.query.kinds === "hanjiCraft") {
        productsInfo = hanjiCraft;
    } else if (req.query.kinds === "knitting") {
        productsInfo = knitting;
    }
    res.render('productPage', {
        title: req.query.kinds,
        productsInfo: productsInfo
    });

};