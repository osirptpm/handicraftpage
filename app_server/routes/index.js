var express = require('express');
var router = express.Router();
const CtrlProduct = require("../controllers/CtrlProduct");

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('mainPage', { title: 'YS Handcraft'});
});
router.get('/intro', function(req, res, next) {
    res.render('intro', { title: "소개" });
});
router.get('/product', CtrlProduct.productsInfo);

module.exports = router;
